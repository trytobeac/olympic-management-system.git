﻿using OlympicManagementSystem.Properties;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OlympicManagementSystem.Util
{
    public class CSVHelper
    {
        public static DataTable ExecuteTable(string filePath)//获取CSV文件的方法，返回一个DataTable
        {
            DataTable dataTable = new DataTable();
            List<string[]> rows = new List<string[]>();
            using (StreamReader sr = new StreamReader(filePath))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] data = line.Split(',');  
                    rows.Add(data);
                }
            }
            //获取列名
            for (int i = 0; i < rows[0].Length; i++)
            {
                dataTable.Columns.Add(rows[0][i]);
            }
            for(int i = 1;i < rows.Count; i++)
            {
                dataTable.Rows.Add(rows[i]);
            }
            //foreach (DataRow dataRow in dataTable.Rows)
            //{
            //    for (int i = 0;i < dataRow.Table.Columns.Count;i++)
            //    {
            //        Console.WriteLine("******CSV校验*******"+dataRow[i]);
            //    }
            //}
            return dataTable;
        }
    }
}

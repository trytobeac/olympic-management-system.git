﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace OlympicManagementSystem.Util
{
    public class SqlHelper
    {
        public static string ConStr { get; set; }//数据库连接字符串

        public static DataTable ExecuteTable(string cmdStr)//获取数据表的方法，返回一个DataTable
        {
            using (SqlConnection con = new SqlConnection(ConStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                return ds.Tables[0];
            }
        }

        public static int ExecuteNonQuery(string cmdStr)//修改数据表的方法，返回受影响行数
        {
            using (SqlConnection con = new SqlConnection(ConStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(cmdStr, con);
                int rows = cmd.ExecuteNonQuery();
                if (rows <= 0)
                {
                    throw new Exception("Database operation failed");//操作失败抛出异常
                }
                return rows;
            }
        }

        public static void ExecutRowInsert(DataRow dataRow,string tableName)
        {
            using (SqlConnection con = new SqlConnection(ConStr))
            {
                con.Open();

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(con))
                {
                    //foreach (DataRow row in dt.Rows)
                    //{
                    //    bulkCopy.DestinationTableName = tableName; //指定数据库中的目标表名  

                    //    //为每一行创建一个DataRowView对象，并将其添加到SqlBulkCopy对象  
                    //    DataRowView rowView = dt.DefaultView[row.RowIndex];
                    //    DataRowView rowView = dataRow.
                    //    bulkCopy.WriteToServer(new DataRowView[] { rowView });
                    //}
                }
            }
        }

    }
}



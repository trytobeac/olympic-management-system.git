﻿using OlympicManagementSystem.Forms;
using OlympicManagementSystem.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OlympicManagementSystem
{
    internal static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {

            //Initialize the database connection string
            SqlHelper.ConStr = ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Login login = new Login();
            DialogResult dialogresult = login.ShowDialog();
            if (dialogresult == DialogResult.OK)
            {
                Application.Run(new Main());
            }
        }
    }
}

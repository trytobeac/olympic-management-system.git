﻿namespace OlympicManagementSystem.Forms
{
    partial class SportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnBack = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.treeViewSports = new System.Windows.Forms.TreeView();
            this.cMSNode = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addSportItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.subheading = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.cMSNode.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(12, 12);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(92, 34);
            this.btnBack.TabIndex = 5;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.treeViewSports);
            this.panel1.Location = new System.Drawing.Point(77, 120);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1014, 464);
            this.panel1.TabIndex = 4;
            // 
            // treeViewSports
            // 
            this.treeViewSports.ContextMenuStrip = this.cMSNode;
            this.treeViewSports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewSports.Location = new System.Drawing.Point(0, 0);
            this.treeViewSports.Name = "treeViewSports";
            this.treeViewSports.Size = new System.Drawing.Size(1014, 464);
            this.treeViewSports.TabIndex = 0;
            this.treeViewSports.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ClickTreeView);
            // 
            // cMSNode
            // 
            this.cMSNode.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addSportItemToolStripMenuItem,
            this.updateToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.cMSNode.Name = "cMSNode";
            this.cMSNode.Size = new System.Drawing.Size(167, 70);
            // 
            // addSportItemToolStripMenuItem
            // 
            this.addSportItemToolStripMenuItem.Name = "addSportItemToolStripMenuItem";
            this.addSportItemToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.addSportItemToolStripMenuItem.Text = "Add Sport Item";
            this.addSportItemToolStripMenuItem.Click += new System.EventHandler(this.addSportItemToolStripMenuItem_Click);
            // 
            // updateToolStripMenuItem
            // 
            this.updateToolStripMenuItem.Name = "updateToolStripMenuItem";
            this.updateToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.updateToolStripMenuItem.Text = "Update";
            this.updateToolStripMenuItem.Click += new System.EventHandler(this.updateToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(454, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(238, 29);
            this.label1.TabIndex = 3;
            this.label1.Text = "Spot Management";
            // 
            // subheading
            // 
            this.subheading.AutoSize = true;
            this.subheading.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.subheading.Location = new System.Drawing.Point(456, 99);
            this.subheading.Name = "subheading";
            this.subheading.Size = new System.Drawing.Size(203, 14);
            this.subheading.TabIndex = 6;
            this.subheading.Text = "Total x sports,y sport items";
            // 
            // SportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1168, 602);
            this.Controls.Add(this.subheading);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Name = "SportForm";
            this.Text = "SportForm";
            this.Load += new System.EventHandler(this.SportForm_Load);
            this.panel1.ResumeLayout(false);
            this.cMSNode.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label subheading;
        private System.Windows.Forms.TreeView treeViewSports;
        private System.Windows.Forms.ContextMenuStrip cMSNode;
        private System.Windows.Forms.ToolStripMenuItem addSportItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
    }
}
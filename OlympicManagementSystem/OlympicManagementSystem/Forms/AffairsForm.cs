﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OlympicManagementSystem.Forms
{
    public partial class AffairsForm : Form
    {
        private Main main;
        public AffairsForm(Main mainForm)
        {
            InitializeComponent();
            main = mainForm;
        }

        private void AffairsForm_Load(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void btnAthlente_Click(object sender, EventArgs e)
        {
            AthlenteForm athlenteForm = new AthlenteForm(main);
            main.LoadSubForm(athlenteForm);

        }

        private void btnVolunteer_Click(object sender, EventArgs e)
        {
            VolunteerForm volunteerForm = new VolunteerForm(main);
            main.LoadSubForm(volunteerForm);
        }
    }
}

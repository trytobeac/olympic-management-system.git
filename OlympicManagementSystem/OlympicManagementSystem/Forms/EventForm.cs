﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OlympicManagementSystem.Forms
{
    public partial class EventForm : Form
    {
        private Main main;
        public EventForm(Main mainForm)
        {
            InitializeComponent();
            main=mainForm;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        public void btnSport_Click(object sender, EventArgs e)//Load SportForm
        {
            SportForm sportForm = new SportForm(main);
            main.LoadSubForm(sportForm);
        }

        private void EventForm_Load(object sender, EventArgs e)
        {

        }

        private void btnCompetition_Click(object sender, EventArgs e)
        {
            CompetitionForm competitionForm = new CompetitionForm(main);
            main.LoadSubForm(competitionForm);
        }
    }
}

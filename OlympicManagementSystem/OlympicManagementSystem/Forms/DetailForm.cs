﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OlympicManagementSystem.Forms
{
    public partial class DetailForm : Form
    {
        DataTable dataTableDetail;
        public DetailForm(DataTable dataTable)
        {
            dataTableDetail = dataTable;
            InitializeComponent();
        }

        private void DetailForm_Load(object sender, EventArgs e)
        {
            dataGridView.DataSource = dataTableDetail;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OlympicManagementSystem.Util;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Button;

namespace OlympicManagementSystem.Forms
{
    public partial class Login : Form
    {
        int roleID = 0;
        int logonCount = 0;
        private const int CodeLength = 4;
        private String verificationCode = "";

        public Login()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)//Determine whether to log in
        {
            logonCount += 1;
            if (logonCount<=3)
            {
                if (loginFlag()) EnterSystem();
                else if (logonCount == 3) UpdateVerificationCode();
            }
            else if (CaptchaVerification()&&loginFlag()) EnterSystem();
        }

        private void EnterSystem()
        {
            DialogResult = DialogResult.OK;
            Remenberme();
        }

        private void btnExit_Click(object sender, EventArgs e)//Exit System
        {
            System.Environment.Exit(0);
        }

        private void Login_Load(object sender, EventArgs e)//Read user information from App.config
        {
            if (ConfigurationManager.AppSettings["remenberme"].Equals("true"))
            {
                txbID.Text = ConfigurationManager.AppSettings["loginID"];
                txbPwd.Text = ConfigurationManager.AppSettings["passwd"];
                chBRemenberMe.Checked = true;
            }
        }

        private void UpdateVerificationCode()//Creat / update verification
        {
            verificationCode = VerificationCode.CreateRandomCode(CodeLength);
            if (verificationCode == "") return;
            VerificationCode.CreateImage(verificationCode, picVCode);
        }

        private bool CaptchaVerification()//Captcha verification
        {
            if (String.IsNullOrEmpty(txbVCode.Text.Trim()) != true)
            {
                if (txbVCode.Text.Trim().ToLower() == verificationCode.ToLower()) return true;
                else
                {
                    MessageBox.Show("验证码错误", "警告", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    UpdateVerificationCode();
                    txbVCode.Text = "";
                    txbVCode.Focus();
                    return false;
                }
            }
            else
            {
                MessageBox.Show("请输入验证码", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                UpdateVerificationCode();
                txbVCode.Text = "";
                txbVCode.Focus();
                return false;
            }
        }

        private void Remenberme()//Write user information
        {
            string loginID = txbID.Text.Trim();
            string passwd = txbPwd.Text.Trim();
            Configuration cf = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (chBRemenberMe.Checked)//Write
            {
                cf.AppSettings.Settings["loginID"].Value = loginID;
                cf.AppSettings.Settings["passwd"].Value = passwd;
                cf.AppSettings.Settings["remenberme"].Value = "true";
            }
            else//initialize
            {
                cf.AppSettings.Settings["loginID"].Value = "";
                cf.AppSettings.Settings["passwd"].Value = "";
                cf.AppSettings.Settings["remenberme"].Value = "false";
            }
            cf.Save();
        }

        private bool loginFlag()//Verify the user information,record roleID and return true or false
        {
            if (txbID.Text == "" || txbPwd.Text == "")
            {
                MessageBox.Show("用户名和密码不能为空", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else
            {//Query user information
                string sql = "select * from Staff where LoginID = '" + txbID.Text + "' and Password = '" + txbPwd.Text + "'";
                DataTable dataTable = SqlHelper.ExecuteTable(sql);
                if (dataTable.Rows.Count > 0)
                {
                    DataRow dataRow = dataTable.Rows[0];
                    roleID = (int)dataRow["RoleID"];
                    Configuration cf = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    cf.AppSettings.Settings["roleID"].Value = roleID.ToString();//record roleID
                    cf.Save();
                    return true;
                }
                else
                {
                    MessageBox.Show("用户名或密码错误", "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
        }

        private void picVCode_Click(object sender, EventArgs e)//Click the image to update the verification code
        {
            if (logonCount>=3) UpdateVerificationCode();
        }
    }
}

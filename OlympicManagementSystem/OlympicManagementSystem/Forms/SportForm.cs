﻿using OlympicManagementSystem.Model;
using OlympicManagementSystem.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OlympicManagementSystem.Forms
{
    public partial class SportForm : Form
    {
        private Main main;
        public SportForm(Main mainForm)
        {
            InitializeComponent();
            main = mainForm;
        }

        private void SportForm_Load(object sender, EventArgs e)
        {
            BandinTreeView(LoadSportModel());
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            main.CloseSubForm();
        }

        private void BandinTreeView(List<SportModel> list)
        {
            List<SportModel> sportModels = list;
            for (int i = 0; i < sportModels.Count; i++)
            {
                TreeNode treeNode = new TreeNode();
                treeNode.Name = sportModels[i].SportID;
                treeNode.Text = sportModels[i].Name;
                treeViewSports.Nodes.Add(treeNode);

                for (int j = 0; j < sportModels[i].SportItem.Rows.Count; j++)//Add subNode
                {
                    DataRow dataRowSportItem = sportModels[i].SportItem.Rows[j];
                    TreeNode treeNodeItem = new TreeNode();
                    treeNodeItem.Name = dataRowSportItem["ItemID"].ToString();
                    treeNodeItem.Text = dataRowSportItem["Name"].ToString();
                    treeNode.Nodes.Add(treeNodeItem);
                }
            }
        }

        private List<SportModel> LoadSportModel()//Load SportModel, count sports and sportitems 
        {
            int sportCount,itemsCount = 0;
            List<SportModel> sportModels = new List<SportModel>();

            string sqlSports = "SELECT SportID, Name FROM Sport";
            DataTable dataTableSport = SqlHelper.ExecuteTable(sqlSports);
            string sportID, name;

            for (int i = 0; i < dataTableSport.Rows.Count; i++)
            {
                DataRow dataRowSport = dataTableSport.Rows[i];
                sportID = dataRowSport["SportID"].ToString();
                name = dataRowSport["Name"].ToString();
                string sqlSportItems = "SELECT ItemID, Name FROM SportItem WHERE SportID = '"+(i+1)+"'";
                DataTable dataTableSportItem = SqlHelper.ExecuteTable(sqlSportItems);
                SportModel sportModel = new SportModel(sportID,name,dataTableSportItem);
                itemsCount+=dataTableSportItem.Rows.Count;//Count items
                sportModels.Add(sportModel);
            }
            sportCount= sportModels.Count;
            subheading.Text = "Total "+sportCount+" sports,"+itemsCount+" sport items";
            return sportModels;
        }

        private void ClickTreeView(object sender, MouseEventArgs e)//右键选中，根据节点等级显示/隐藏功能菜单
        {
            if (e.Button == MouseButtons.Right)//右键点击
            {
                Point clickPoint = new Point(e.X, e.Y);
                TreeNode clickNode = treeViewSports.GetNodeAt(clickPoint);
                if (clickNode != null)
                {
                    treeViewSports.SelectedNode = clickNode;
                    if (clickNode.Level == 0)
                    {
                        cMSNode.Items[0].Visible = true;
                        cMSNode.Items[1].Visible = false;
                        cMSNode.Items[2].Visible = false;
                    }
                    else
                    {
                        cMSNode.Items[0].Visible = false;
                        cMSNode.Items[1].Visible = true;
                        cMSNode.Items[2].Visible = true;
                    }
                }
            }
        }
        
        private void addSportItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string sportID, name;
            TreeNode treeNode = treeViewSports.SelectedNode;
            
            //弹出输入框
            InputDialog inputDialog = new InputDialog();
            inputDialog.ShowDialog();
            if (inputDialog.DialogResult == DialogResult.OK && inputDialog.userInput != "")
            {
                sportID = treeNode.Name;
                name = inputDialog.userInput;
                TreeNode treeNodeItem = new TreeNode();
                treeNodeItem.Text = name;
                treeNode.Nodes.Add(treeNodeItem);
                string sql = "INSERT INTO SportItem(SportID, Name) VALUES ('"+sportID+"','"+name+"')";
                if (SqlHelper.ExecuteNonQuery(sql) == 1) MessageBox.Show("添加成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else MessageBox.Show("内容不能为空","Node Label Edit");
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string sportID, name;
            TreeNode treeNodeItem = treeViewSports.SelectedNode;
            sportID = treeNodeItem.Parent.Name;
            name = treeNodeItem.Text;
            InputDialog inputDialog = new InputDialog();
            inputDialog.ShowDialog();
            if (inputDialog.DialogResult == DialogResult.OK && inputDialog.userInput != "")
            {
                if (inputDialog.userInput != name)
                {
                    treeNodeItem.Text = inputDialog.userInput;
                    string sql = "UPDATE SportItem SET Name = '"+inputDialog.userInput+"' WHERE SportID = '"+sportID+"' and Name = '"+name+"'";
                    if (SqlHelper.ExecuteNonQuery(sql) == 1) MessageBox.Show("添加成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else MessageBox.Show("重命名内容一致", "Node Label Edit", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else MessageBox.Show("内容不能为空", "Node Label Edit");

        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string sportID, name;
            TreeNode treeNode = treeViewSports.SelectedNode;
            sportID = treeNode.Parent.Name;
            name = treeNode.Text;
            string sql = "DELETE FROM SportItem WHERE SportID = '"+sportID+"' and Name = '"+name+"'";
            if (SqlHelper.ExecuteNonQuery(sql) == 1)
            {
                treeNode.Remove();
                MessageBox.Show("删除成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }else MessageBox.Show("未知错误", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}

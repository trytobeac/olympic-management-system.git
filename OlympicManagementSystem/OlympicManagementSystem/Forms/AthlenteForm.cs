﻿using OlympicManagementSystem.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition.Primitives;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OlympicManagementSystem.Forms
{
    public partial class AthlenteForm : Form
    {
        private Main main;
        DataTable dataTable = new DataTable();
        public AthlenteForm(Main mainForm)
        {
            InitializeComponent();
            main = mainForm;
        }

        private void AthlenteForm_Load(object sender, EventArgs e)
        {
            List<string> gender = new List<string>();
            gender.Add("--ALL--");
            gender.Add("F");
            gender.Add("M");
            combGender.DataSource = gender;

            string sqlCountry, sqlSport;
            sqlCountry = "SELECT CountryName Name FROM Country";
            sqlSport = "SELECT Name FROM Sport";
            BindCombbox(combCountry,sqlCountry);
            BindCombbox(comboSport, sqlSport);

            LoadDataGridView();
        }

        private void LoadDataGridView()
        {
            string sql = "SELECT a.FirstName,a.LastName,a.Gender,c.CountryName Country,a.Passport,s.Name Sport,a.BirthDate,a.Height,a.Weight " +
                "FROM Athlete a,Country c,Sport s " +
                "WHERE a.CountryCode = c.CountryCode AND a.SportID = s.SportID";
            dataTable = SqlHelper.ExecuteTable(sql);
            dataGridView.DataSource = dataTable;
        }

        private void btnBack_Click(object sender, EventArgs e) 
        {
            main.CloseSubForm();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            //get user input
            string[] name;
            string filterStr = "";
            string country, gender, sport;
            country = combCountry.Text;
            gender = combGender.Text;
            sport = comboSport.Text;
            Console.WriteLine(country+gender+sport);
            if (txtName.Text.Trim() != string.Empty) { 
                name = txtName.Text.Trim().Split(' ');
                if (name.Length > 1)
                {
                    filterStr = "FirstName = '"+name[0]+"' AND LastName = '"+name[1]+"' AND (Country = '"+country+"' OR '"+country+"' = '--ALL--')" +
                    " AND (Gender = '"+gender+"' OR '"+gender+"' = '--ALL--')" +
                    " AND (Sport = '"+sport+"' OR '"+sport+"' = '--ALL--')";
                }
                else {
                    filterStr = "(FirstName = '"+name[0]+"' OR LastName = '"+name[0]+"') AND (Country = '"+country+"' OR '"+country+"' = '--ALL--')" +
                        " AND (Gender = '"+gender+"' OR '"+gender+"' = '--ALL--')" +
                        " AND (Sport = '"+sport+"' OR '"+sport+"' = '--ALL--')";
                } 
            }
            else
            {
                filterStr = "(Country = '"+country+"' OR '"+country+"' = '--ALL--')" +
                    " AND (Gender = '"+gender+"' OR '"+gender+"' = '--ALL--')" +
                    " AND (Sport = '"+sport+"' OR '"+sport+"' = '--ALL--')";
            }

            DataTable dataTableFilter = dataTable.Clone();
            DataRow[] rows = dataTable.Select(filterStr); // 从dt 中查询符合条件的记录； 
            foreach (DataRow row in rows)  // 将查询的结果添加到dt中； 
            {
                dataTableFilter.Rows.Add(row.ItemArray);
            }
            dataGridView.DataSource = dataTableFilter;
            Console.WriteLine(filterStr);
        }

        private void BindCombbox(ComboBox comboBox,string sql)
        {
            DataTable dataTable = SqlHelper.ExecuteTable(sql);
            List<string> combItems = new List<string>();
            combItems.Add("--ALL--");
            for (int i = 0; i<dataTable.Rows.Count; i++)
            {
                combItems.Add(dataTable.Rows[i]["Name"].ToString());
            }
            comboBox.DataSource = combItems;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int a = dataGridView.CurrentCell.RowIndex;
            string[] values = new string[dataGridView.Rows[a].Cells.Count];
            for (int i = 0; i < dataGridView.Rows[a].Cells.Count; i++)
            {
                values[i] = dataGridView.Rows[a].Cells[i].Value.ToString();
            }
            string sql = "DELETE FROM Athlete WHERE FirstName = '"+values[0]+"' AND LastName = '"+values[1]+"'" +
                " AND Gender = '"+values[2]+"' AND BirthDate = '"+values[6]+"'";
            if (SqlHelper.ExecuteNonQuery(sql) >= 1)
            {
                MessageBox.Show("删除成功");
                LoadDataGridView();
            }
            else
            {
                MessageBox.Show("执行失败，未知错误");
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            ImportAthleteForm frm = new ImportAthleteForm();
            frm.Show();
        }
    }
}

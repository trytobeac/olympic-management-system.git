﻿namespace OlympicManagementSystem.Forms
{
    partial class ImportAthleteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBack = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.linkLabDownload = new System.Windows.Forms.LinkLabel();
            this.labImported = new System.Windows.Forms.Label();
            this.labDiscarded = new System.Windows.Forms.Label();
            this.linkLabViewDetail = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(12, 12);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(92, 34);
            this.btnBack.TabIndex = 6;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(283, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(223, 29);
            this.label1.TabIndex = 7;
            this.label1.Text = "Import Athlete";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(82, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "Athlete files:";
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(210, 89);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.ReadOnly = true;
            this.txtFilePath.Size = new System.Drawing.Size(296, 21);
            this.txtFilePath.TabIndex = 9;
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Location = new System.Drawing.Point(522, 88);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(75, 23);
            this.btnOpenFile.TabIndex = 10;
            this.btnOpenFile.Text = " . . .";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(210, 140);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(75, 23);
            this.btnImport.TabIndex = 11;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(483, 140);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // linkLabDownload
            // 
            this.linkLabDownload.AutoSize = true;
            this.linkLabDownload.Location = new System.Drawing.Point(619, 93);
            this.linkLabDownload.Name = "linkLabDownload";
            this.linkLabDownload.Size = new System.Drawing.Size(107, 12);
            this.linkLabDownload.TabIndex = 13;
            this.linkLabDownload.TabStop = true;
            this.linkLabDownload.Text = "Download Template";
            this.linkLabDownload.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabDownload_LinkClicked);
            // 
            // labImported
            // 
            this.labImported.AutoSize = true;
            this.labImported.Location = new System.Drawing.Point(115, 227);
            this.labImported.Name = "labImported";
            this.labImported.Size = new System.Drawing.Size(131, 12);
            this.labImported.TabIndex = 14;
            this.labImported.Text = "Imported records:[XX]";
            // 
            // labDiscarded
            // 
            this.labDiscarded.AutoSize = true;
            this.labDiscarded.Location = new System.Drawing.Point(421, 227);
            this.labDiscarded.Name = "labDiscarded";
            this.labDiscarded.Size = new System.Drawing.Size(137, 12);
            this.labDiscarded.TabIndex = 15;
            this.labDiscarded.Text = "Discarded records:[XX]";
            // 
            // linkLabViewDetail
            // 
            this.linkLabViewDetail.AutoSize = true;
            this.linkLabViewDetail.Location = new System.Drawing.Point(570, 226);
            this.linkLabViewDetail.Name = "linkLabViewDetail";
            this.linkLabViewDetail.Size = new System.Drawing.Size(71, 12);
            this.linkLabViewDetail.TabIndex = 16;
            this.linkLabViewDetail.TabStop = true;
            this.linkLabViewDetail.Text = "View Detail";
            this.linkLabViewDetail.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabViewDetail_LinkClicked);
            // 
            // ImportAthleteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 266);
            this.Controls.Add(this.linkLabViewDetail);
            this.Controls.Add(this.labDiscarded);
            this.Controls.Add(this.labImported);
            this.Controls.Add(this.linkLabDownload);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.btnOpenFile);
            this.Controls.Add(this.txtFilePath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnBack);
            this.Name = "ImportAthleteForm";
            this.Text = "ImportAthleteForm";
            this.Load += new System.EventHandler(this.ImportAthleteForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFilePath;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.LinkLabel linkLabDownload;
        private System.Windows.Forms.Label labImported;
        private System.Windows.Forms.Label labDiscarded;
        private System.Windows.Forms.LinkLabel linkLabViewDetail;
    }
}
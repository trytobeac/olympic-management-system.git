﻿using OlympicManagementSystem.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OlympicManagementSystem.Forms
{
    public partial class CompetitionForm : Form
    {
        private Main main;
        public CompetitionForm(Main mainForm)
        {
            InitializeComponent();
            this.main = mainForm;
        }

        private void CompetitionForm_Load(object sender, EventArgs e)
        {
            LoadButtons();
        }

        private void LoadButtons()
        {
            string sql = "SELECT SportID,Name FROM Sport";
            DataTable dataTable = new DataTable();
            dataTable =  SqlHelper.ExecuteTable(sql);

            for(int i = 0; i<dataTable.Rows.Count; i++)
            {
                Button button = new Button();
                button.Parent = flowLayoutPanel;
                button.Text = dataTable.Rows[i]["Name"].ToString();
                button.Name = dataTable.Rows[i]["SportID"].ToString();
                button.Size = new Size(160, 60);
                button.Enabled = false;

                if (dataTable.Rows[i]["Name"].ToString() == "Football")
                {
                    button.Enabled = true;
                    button.Click += new EventHandler(btnFootball_Click);
                }
                if (dataTable.Rows[i]["Name"].ToString() == "Shooting")
                {
                    button.Enabled = true;
                    button.Click += new EventHandler(btnShooting_Click);
                }
            }
        }

        private void btnShooting_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Shooting");
        }

        private void btnFootball_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Football");
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            main.CloseSubForm();
        }
    }
}

﻿using OlympicManagementSystem.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace OlympicManagementSystem.Forms
{
    public partial class VolunteerForm : Form
    {
        private Main main;
        DataTable dataTable = new DataTable();
        public VolunteerForm(Main mainForm)
        {
            InitializeComponent();
            main = mainForm;
        }

        private void VolunteerForm_Load(object sender, EventArgs e)
        {
            combClass.Items.Add("Country");
            combClass.Items.Add("Gender");
            combClass.Items.Add("Sport");
            combClass.Items.Add("Education");
            combClass.SelectedIndex = 0;
            chartView.Dock = DockStyle.Fill;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            main.CloseSubForm();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string sql = "";
            switch (combClass.Text)
            {
                case "Country":
                    sql = "SELECT c.CountryName, COUNT(*) as count FROM Volunteer v,Country c " +
                        "WHERE V.CountryCode = c.CountryCode GROUP BY v.CountryCode,c.CountryName;";
                    break;
                case "Sport":
                    sql = "SELECT s.Name,COUNT(*) as count FROM Volunteer v,Sport s " +
                        "WHERE v.SportID = s.SportID GROUP BY v.SportID,s.Name";
                    break;
                case "Gender":
                case "Education":
                    sql = "SELECT "+combClass.Text+", COUNT(*) as count FROM Volunteer GROUP BY "+combClass.Text+";";
                    break;
                default:
                    MessageBox.Show("未知错误");
                    break;
            }
            dataTable = SqlHelper.ExecuteTable(sql);
            LandDataGridView();
            LandChart();
        }
        private void LandDataGridView()
        {
            dataGridView.DataSource = dataTable;
        }

        private void LandChart()
        {
            chartView.Series.Clear();

            // 创建柱状图类型的Series对象
            Series series = new Series();
            series.ChartType = SeriesChartType.Column;

            foreach (DataRow row in dataTable.Rows)
            {
                series.Points.AddXY(row[0], row[1]);
            }
            // 将Series对象添加到Chart控件中
            chartView.Series.Add(series);
        }
    }
}

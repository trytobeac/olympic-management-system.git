﻿using OlympicManagementSystem.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Data.SqlClient;
using System.Configuration;
using OlympicManagementSystem.Forms;

namespace OlympicManagementSystem
{
    public partial class Main : Form
    {
        int roleID = 0;
        Form menuForm = new Form();
        Form subForm = new Form();
        
        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            //Read roleID from App.config and creat menuForm
            Configuration cf = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            roleID = int.Parse(cf.AppSettings.Settings["roleID"].Value);
            LoadMenuForm(roleID);
        }

        private void LoadMenuForm(int role)//To load the menu form based on roleID, need to pass in roleID
        {
            switch (role)
            {
                case 0:
                    MessageBox.Show("获取RoleID失败", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break; 
                case 1:
                    menuForm = new AffairsForm(this);
                    break; 
                case 2:
                    menuForm = new EventForm(this);
                    break; 
                case 3:
                    menuForm = new StatisticsForm();
                    break;
                default:
                    MessageBox.Show("加载窗体未知错误", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
            menuForm.TopLevel = false;
            menuForm.FormBorderStyle = FormBorderStyle.None;
            menuForm.Dock = DockStyle.Fill;
            this.panMain.Controls.Clear();
            this.panMain.Controls.Add(menuForm);
            menuForm.Show();
        }

        public void LoadSubForm(Form form)//Load subForm, need to pass in Form object
        {
            subForm = form;
            menuForm.Hide();
            subForm.TopLevel = false;
            subForm.FormBorderStyle = FormBorderStyle.None;
            subForm.Dock = DockStyle.Fill;
            this.panMain.Controls.Add(subForm);
            subForm.Show();
        }

        public void CloseSubForm()//Close SubForm and show MenuForm
        {
            subForm.Close();
            menuForm.Show();
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Net;
using System.Net.Http;
using System.Resources;
using OlympicManagementSystem.Properties;
using OlympicManagementSystem.Util;

namespace OlympicManagementSystem.Forms
{
    public partial class ImportAthleteForm : Form
    {
        DataTable dataTableDetil;
        List<string> countryCode = new List<string>();

        public ImportAthleteForm()
        {
            InitializeComponent();
        }

        private void linkLabDownload_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string selectedPath = string.Empty;
            string FilePath = string.Empty;
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "请选择Txt所在文件夹";
            if (dialog.ShowDialog()==DialogResult.OK)
            {
                if (string.IsNullOrEmpty(dialog.SelectedPath))
                {
                    MessageBox.Show(this, "文件夹路径不能为空", "提示");
                    return;
                }
                selectedPath = dialog.SelectedPath;
                FilePath = selectedPath+"\\Import_athlete_template.csv";
                System.IO.File.WriteAllText(FilePath, Properties.Resources.Import_athlete_template);
                MessageBox.Show("文件下载成功");
            }
            else MessageBox.Show("文件下载失败");
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;//该值确定是否可以选择多个文件
            dialog.Title = "请选择文件夹";
            dialog.Filter = "所有文件(*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = dialog.FileName;
                txtFilePath.Text = filePath;
            }
        }
        private void btnImport_Click(object sender, EventArgs e)
        {
            DataTable csvData = new DataTable();
            if (txtFilePath.Text != string.Empty)
            {
                string filePath = txtFilePath.Text;
                csvData = CSVHelper.ExecuteTable(filePath).Copy();
                MessageBox.Show("导入成功");
            }
            else MessageBox.Show("请选择文件");

            InsertintoDatabase(csvData);
        }

        private void InsertintoDatabase(DataTable dataTable)
        {
            int succeedCount = 0;
            int failedCount = 0;
            dataTableDetil = dataTable.Copy();
            dataTableDetil.Columns.Add("Status");
            dataTableDetil.Columns.Add("Detail");
            int columnsCount = dataTable.Columns.Count;
            int rowsCount = dataTable.Rows.Count;
            

            for (int i = 0;i < rowsCount;i++)
            {
                bool continueFlag = false;
                for (int j = 0; j < columnsCount; j++)
                {
                    if (dataTable.Rows[i][j].ToString()==string.Empty)
                    {
                        dataTableDetil.Rows[i]["Status"] = "Failed";
                        dataTableDetil.Rows[i]["Detail"] = "Value is empty";
                        continueFlag = true;
                        break;
                    }
                }
                if (continueFlag)
                {
                    failedCount++;
                    continue;
                }
                bool a, b, c, d;//判断是否为正确格式
                a = int.TryParse(dataTable.Rows[i]["SportID"].ToString(), out int sportID);
                b = DateTime.TryParse(dataTable.Rows[i]["BirthDate"].ToString(), out DateTime birthDate);
                c = float.TryParse(dataTable.Rows[i]["Height"].ToString(), out float Height);
                d = float.TryParse(dataTable.Rows[i]["Weight"].ToString(), out float Weight);
                if (a&&b&&c&&d)
                {
                    //排除重复（护照号，国家）
                    string selectSql = "SELECT * FROM Athlete WHERE CountryCode = '"
                        +dataTable.Rows[i]["CountryCode"].ToString()+"' AND Passport = '"+dataTable.Rows[i]["Passport"]+"';";
                    DataTable athlete = SqlHelper.ExecuteTable(selectSql);
                    if (athlete.Rows.Count<1)
                    {
                        //不重复
                        if (countryCode.Contains(dataTable.Rows[i]["CountryCode"].ToString()))
                        {
                            //包含，国家合法
                            string sql = "INSERT INTO Athlete(FirstName,LastName,Gender,Phone,Email,CountryCode,Passport,SportID,BirthDate,Height,Weight) ";
                            string sqlValues = "VALUES ();";
                            dataTableDetil.Rows[i]["Status"] = "Succeed";
                            succeedCount++;
                        }
                        else
                        {
                            //CountryCode 错误
                            dataTableDetil.Rows[i]["Status"] = "Failed";
                            dataTableDetil.Rows[i]["Detail"] = "Country code error";
                            failedCount++;
                            continue;
                        }
                    }
                    else
                    {
                        //信息重复
                        dataTableDetil.Rows[i]["Status"] = "Failed";
                        dataTableDetil.Rows[i]["Detail"] = "Data duplication";
                        failedCount++;
                        continue;
                    }
                    
                    
                }
                else
                {
                    //格式不正确
                    dataTableDetil.Rows[i]["Status"] = "Failed";
                    dataTableDetil.Rows[i]["Detail"] = "Format is incorrect";
                    failedCount++;
                }

            }
            //统计计数
            labImported.Text = "Imported records:["+succeedCount+"]";
            labDiscarded.Text = "Discarded records: ["+failedCount+"]";
        }
        private void linkLabViewDetail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //显示详情
            DetailForm detailForm = new DetailForm(dataTableDetil);
            detailForm.Show();
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ImportAthleteForm_Load(object sender, EventArgs e)
        {
            //获取CountryCode
            DataTable dataTable = SqlHelper.ExecuteTable("SELECT CountryCode FROM Country");
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                countryCode.Add(dataTable.Rows[i][0].ToString());
            }
        }
    }
}

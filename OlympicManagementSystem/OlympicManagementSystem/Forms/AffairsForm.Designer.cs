﻿namespace OlympicManagementSystem.Forms
{
    partial class AffairsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnVolunteer = new System.Windows.Forms.Button();
            this.btnAthlente = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(365, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(403, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Affairs Administrator Menu";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnVolunteer);
            this.panel1.Controls.Add(this.btnAthlente);
            this.panel1.Location = new System.Drawing.Point(83, 85);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1014, 416);
            this.panel1.TabIndex = 1;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(514, 527);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(92, 34);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnVolunteer
            // 
            this.btnVolunteer.Location = new System.Drawing.Point(612, 152);
            this.btnVolunteer.Name = "btnVolunteer";
            this.btnVolunteer.Size = new System.Drawing.Size(240, 85);
            this.btnVolunteer.TabIndex = 3;
            this.btnVolunteer.Text = "Volunteer Management";
            this.btnVolunteer.UseVisualStyleBackColor = true;
            this.btnVolunteer.Click += new System.EventHandler(this.btnVolunteer_Click);
            // 
            // btnAthlente
            // 
            this.btnAthlente.Location = new System.Drawing.Point(153, 152);
            this.btnAthlente.Name = "btnAthlente";
            this.btnAthlente.Size = new System.Drawing.Size(240, 85);
            this.btnAthlente.TabIndex = 2;
            this.btnAthlente.Text = "Athlente Management";
            this.btnAthlente.UseVisualStyleBackColor = true;
            this.btnAthlente.Click += new System.EventHandler(this.btnAthlente_Click);
            // 
            // AffairsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1168, 602);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Name = "AffairsForm";
            this.Text = "AffairsForm";
            this.Load += new System.EventHandler(this.AffairsForm_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnVolunteer;
        private System.Windows.Forms.Button btnAthlente;
    }
}
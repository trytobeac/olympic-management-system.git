﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlympicManagementSystem.Model
{
    class SportModel
    {
        private string _SportID;
        private string _Name;
        private DataTable _SportItem;

        public SportModel(string SportID, string Name,DataTable SportItem)
        {
            this._SportID = SportID;
            this._Name = Name;
            this._SportItem = SportItem;
        }

        public string Name { get { return _Name; } }
        public string SportID { get { return _SportID; } }
        public DataTable SportItem { get { return _SportItem; } }
    }
}
